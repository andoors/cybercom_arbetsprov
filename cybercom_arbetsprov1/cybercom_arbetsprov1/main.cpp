#include <vector>
#include <iostream>
#include <random>

using namespace std;

// The order represents the rank (from lowest to highest)
vector<char> aDeckOfCards = { '2','3','4','5','6','7','8','9','T','J','Q','K','A' };

int getCardValue(char card) {
	for (int i = 0; i < aDeckOfCards.size(); i++) {
		if (card == aDeckOfCards[i]) {
			return i;
		}
	}
	cout << "Someone tries to cheat\n";
	return -1;
}

vector<int> winner(vector<char>* deck1, vector<char>* deck2) {

	vector<int> deckScores = {0, 0};

	int i = 0;
	while (i < deck1->size() && i < deck2->size()) {

		int deck1CardValue = getCardValue((*deck1)[i]);
		int deck2CardValue = getCardValue((*deck2)[i]);

		if (deck1CardValue > deck2CardValue)
			deckScores[0]++;
		else if (deck1CardValue < deck2CardValue)
			deckScores[1]++;

		i++;
	}

	return deckScores;
}

void populateDeck(vector<char>* deck, int nrOfCards) {
	deck->clear();
	for (int i = 0; i < nrOfCards; i++) {
		int cardId = rand() % aDeckOfCards.size();
		deck->push_back(aDeckOfCards[cardId]);
	}
}

int main() {
	vector<char> deckSteve;
	vector<char> deckJosh;

	int nrOfDealtCards = 9;

	populateDeck(&deckSteve, nrOfDealtCards);
	populateDeck(&deckJosh, nrOfDealtCards);

	vector<int> deckScores = winner(&deckSteve, &deckJosh);
	if (deckScores[0] > deckScores[1])
		cout << "Steve wins " << deckScores[0] << " to " << deckScores[1] << "\n";
	else if (deckScores[0] < deckScores[1])
		cout << "Josh wins " << deckScores[1] << " to " << deckScores[0] << "\n";
	else
		cout << "Tie\n";

	cin >> nrOfDealtCards;

	return 1;
}